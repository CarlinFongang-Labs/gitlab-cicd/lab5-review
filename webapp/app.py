import os

from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello world! Welcome, I am Carlinfg, I design the color web page'

# Nouvelle route pour changer la couleur de la page
@app.route('/change_color')
def change_color():
    # Changement de la couleur de la page
    colored_page = '<html><body style="background-color: lightblue;">Color changed!</body></html>'
    return colored_page

if __name__ == '__main__':
    app.run(host='0.0.0.0')
